import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'service/app_state.dart';
import 'main_controller.dart';

void main() {
  return runApp(
    ChangeNotifierProvider<AppStateModel>(
      create: (_) => AppStateModel()..loadMessages(),
      child: ChatAppHomePage(),
    ),
  );
}
