import 'package:rtm/model/user.dart';

class Conversation {
  const Conversation({this.reciever, this.key});

  final User reciever;
  final String key;

  factory Conversation.fromJson(json) => Conversation(
        reciever: User.fromJson(json["reciever"]),
        key: json["key"],
      );

  @override
  String toString() {
    return '{"key": "$key", "reciever": ${reciever.toString()}}';
  }

  factory Conversation.fromSnapshot(snapshot) => Conversation(
        reciever: User.fromJson(snapshot.value),
        key: snapshot.key,
      );

  Map<String, dynamic> toJson() => {
        'name': reciever.name,
        'key': reciever.key,
      };
}
