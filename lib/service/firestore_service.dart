import 'package:firebase_database/firebase_database.dart';
import 'package:rtm/model/conversation.dart';
import 'package:rtm/model/message.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirestoreService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  User user = FirebaseAuth.instance.currentUser;

  Future<void> addMessage(Message message) async {
    final databaseReference = FirebaseDatabase.instance.reference();
    databaseReference.child("messages").push().set(
          message.toJson(),
        );
  }

  Future<DataSnapshot> getConversation(Conversation conversation) {
    final databaseReference = FirebaseDatabase.instance.reference();
    return databaseReference
        .child("conversations")
        .child(user.uid)
        .orderByChild("name")
        .equalTo(conversation.reciever.name)
        .once();
  }

  Conversation createOrGetConversation(Conversation conversation) {
    Conversation conversation1;
    final databaseReference = FirebaseDatabase.instance.reference();
    databaseReference
        .child("conversations")
        .child(user.uid)
        .orderByChild("name")
        .equalTo(conversation.reciever.name)
        .once()
        .then((value) => {
              if (value.value != null)
                {
                  conversation1 = Conversation.fromSnapshot(value),
                }
              else
                {
                  databaseReference
                      .child("conversations")
                      .child(user.uid)
                      .push()
                      .set(
                        conversation.toJson(),
                      ),
                }
            });

    if (conversation1 == null) {
      databaseReference
          .child("conversations")
          .child(user.uid)
          .orderByChild("name")
          .equalTo(conversation.reciever.name)
          .once()
          .then((value) => {
                if (value != null)
                  {
                    conversation1 = Conversation.fromSnapshot(value),
                  }
              });
    }

    return conversation1;
  }

  userIsLoggedIn() {
    FirebaseAuth auth = FirebaseAuth.instance;
    auth.authStateChanges().listen((User user) {
      if (user == null) {
        print('User is currently signed out!');
        signInWithGoogle();
      } else {
        print('User is signed in!');
      }
    });
  }

  Future<UserCredential> signInWithGoogle() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      print('signInWithGoogle succeeded: $user');
    }

    return null;
  }
}
