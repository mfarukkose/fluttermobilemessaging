import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rtm/message_tile.dart';
import 'package:rtm/model/conversation.dart';
import 'package:rtm/service/app_state.dart';
import 'package:rtm/service/firestore_service.dart';
import 'model/message.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  Query _ref;
  List<Message> messageList = new List();
  User user = FirebaseAuth.instance.currentUser;
  Conversation conversation;
  AppStateModel appStateModel = new AppStateModel();
  TextEditingController messageController = new TextEditingController();
  FirestoreService firestoreService = new FirestoreService();

  @override
  void initState() {
    appStateModel.getConversation().then((value) => {
          conversation = value,
          _ref = FirebaseDatabase.instance
              .reference()
              .child("messages")
              .orderByChild("conversationId")
              .equalTo(conversation.key),
          _ref.onChildAdded.listen(_onEntryAdded),
        });

    super.initState();
  }

  _onEntryAdded(Event event) {
    setState(() {
      Message m = Message.fromSnapshot(event.snapshot);
      messageList.add(m);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text(conversation == null ? "" : this.conversation.reciever.name),
      ),
      body: Container(
        child: SafeArea(
          child: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    flex: 9,
                    child: chatWidget(),
                  ),
                  Expanded(
                    flex: 1,
                    child: chatInputArea(),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Container chatInputArea() {
    return Container(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 5),
        child: Row(
          children: [
            Expanded(
              child: TextField(
                style: TextStyle(
                  height: 1.0,
                ),
                controller: messageController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  filled: true,
                  fillColor: Color(0xFFF2F2F2),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(width: 1, color: Colors.green),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Message willSendMessage = Message.fromJson({
                  "senderId": user.uid,
                  "receiverId": conversation.reciever.key,
                  "conversationId": conversation.key,
                  "message": messageController.text,
                  "date": DateFormat('yyyy-MM-dd – kk:mm')
                      .format(DateTime.now())
                      .toString(),
                  "isReaded": false
                });

                firestoreService.addMessage(willSendMessage);
                messageController.clear();
              },
              child: Container(
                height: 40,
                margin: EdgeInsets.only(left: 10),
                width: 40,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.red, Colors.black],
                  ),
                  borderRadius: BorderRadius.circular(40),
                ),
                padding: EdgeInsets.all(12),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget chatWidget() {
    return messageList != null
        ? _buildSuggestions()
        : Center(
            child: CircularProgressIndicator(),
          );
  }

  Widget _buildSuggestions() {
    // return FirebaseAnimatedList(
    //   query: _ref,
    //   itemBuilder: (
    //     BuildContext context,
    //     DataSnapshot snaphot,
    //     Animation<double> animation,
    //     int index,
    //   ) {
    //     return MessageTile(
    //       snaphot.value,
    //       conversation,
    //     );
    //   },
    // );

    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemCount: messageList.length,
      shrinkWrap: false,
      itemBuilder: (context, i) {
        return MessageTile(
          messageList[i],
          user,
        );
      },
    );
  }
}
