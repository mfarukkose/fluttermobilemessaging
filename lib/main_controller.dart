import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rtm/conversation_list.dart';
import 'package:firebase_core/firebase_core.dart';

class ChatAppHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Black Box',
      theme: ThemeData(
        primaryColor: Color.fromARGB(255, 222, 142, 82),
      ),
      home: FutureBuilder(
        // Initialize FlutterFire
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          // Check for errors
          if (snapshot.hasError) {
            return Text("data");
          }

          // Once complete, show your application
          if (snapshot.connectionState == ConnectionState.done) {
            return ConversationList();
          }

          // Otherwise, show something whilst waiting for initialization to complete
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

// Theme.of(context).platform == TargetPlatform.iOS ? xxx : xxx
