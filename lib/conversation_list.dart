import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:rtm/chat.dart';
import 'package:rtm/model/conversation.dart';
import 'package:rtm/service/app_state.dart';
import 'package:rtm/service/firestore_service.dart';
import 'package:rtm/service/search_conversation.dart';

class ConversationList extends StatefulWidget {
  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  FirestoreService firestoreService = new FirestoreService();
  User user = FirebaseAuth.instance.currentUser;
  Query dbRef;
  List<Conversation> conversationList = new List();
  AppStateModel appStateModel = new AppStateModel();
  @override
  void initState() {
    firestoreService.userIsLoggedIn();
    dbRef = FirebaseDatabase.instance
        .reference()
        .child("conversations")
        .child(user.uid);

    dbRef.onChildAdded.listen(_onEntryAdded);
    dbRef.onChildChanged.listen(_onEntryChanged);
    super.initState();
  }

  _onEntryAdded(Event event) {
    setState(() {
      conversationList.add(Conversation.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var oldEntry = conversationList.singleWhere((entry) {
      return entry.reciever == event.snapshot.value;
    });

    setState(() {
      conversationList[conversationList.indexOf(oldEntry)] =
          Conversation.fromJson(event.snapshot);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Black Box'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              //   Navigator.push(
              //     context,
              //     MaterialPageRoute<void>(
              //       builder: (BuildContext context) {
              //         return AddConversation();
              //       },
              //     ),
              //   );

              showSearch(context: context, delegate: SearchConversatiton());
            },
          ),
        ],
      ),
      body: Container(
        child: SafeArea(
          child: _buildSuggestions(conversationList),
        ),
      ),
    );
  }

  Widget _buildSuggestions(List<Conversation> conversationlist) {
    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemCount: conversationlist.length,
      shrinkWrap: false,
      itemBuilder: (context, i) => ListTile(
        onTap: () {
          appStateModel.saveConversation(conversationlist[i]);
          Navigator.push(
            context,
            MaterialPageRoute<void>(
              builder: (context) => Chat(),
            ),
          );
        },
        leading: Icon(Icons.person),
        title: Text(conversationlist[i].reciever.name),
      ),
    );
  }
}
