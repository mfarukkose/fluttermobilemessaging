class User {
  const User({
    this.key,
    this.name,
    this.phoneNumber,
  });

  final String key;
  final String name;
  final String phoneNumber;

  factory User.fromJson(json) => User(
        key: json["key"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
      );

  @override
  String toString() {
    return '{"key": "$key", "name": "$name", "phoneNumber": "$phoneNumber"}';
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'phoneNumber': phoneNumber,
      };
}
