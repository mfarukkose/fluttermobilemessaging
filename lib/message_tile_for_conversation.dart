import 'package:flutter/material.dart';
import 'package:rtm/model/conversation.dart';

class MessageTileForConversation extends StatelessWidget {
  final Conversation conversation;

  MessageTileForConversation(this.conversation);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(12),
      margin: EdgeInsets.all(5),
      alignment: Alignment.center,
      child: Container(
        width: size.width,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(29),
          color: Colors.red,
        ),
        child: Text(
          conversation.reciever.name,
          style: TextStyle(
            fontSize: 25.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
