import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField(
      {Key key,
      @required this.label,
      @required this.iconData,
      @required this.onChanged})
      : super(key: key);
  final String label;
  final IconData iconData;
  final ValueChanged<String> onChanged;

  @override
  Widget build(BuildContext context) {
    return TextContainer(
      child: InputField(
        label: label,
        iconData: iconData,
        onChanged: onChanged,
      ),
    );
  }
}

class InputField extends StatelessWidget {
  const InputField(
      {Key key,
      @required this.label,
      @required this.iconData,
      @required this.onChanged})
      : super(key: key);

  final String label;
  final IconData iconData;
  final ValueChanged<String> onChanged;

  @override
  Widget build(BuildContext context) {
    return new TextField(
      onChanged: onChanged,
      decoration: InputDecoration(
        icon: Icon(
          iconData,
          color: Color.fromARGB(255, 222, 142, 82),
        ),
        hintText: label,
        border: InputBorder.none,
      ),
    );
  }
}

class TextContainer extends StatelessWidget {
  const TextContainer({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.7,
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 245, 220, 82),
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
