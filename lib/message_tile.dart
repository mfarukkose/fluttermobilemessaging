import 'package:flutter/material.dart';
import 'package:rtm/model/message.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MessageTile extends StatelessWidget {
  final Message message;
  final User user;
  MessageTile(this.message, this.user);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: message.senderId == user.uid ? 0 : 12,
        right: message.senderId == user.uid ? 12 : 0,
      ),
      margin: EdgeInsets.symmetric(vertical: 5),
      alignment: message.senderId == user.uid
          ? Alignment.centerRight
          : Alignment.centerLeft,
      child: Container(
        width: MediaQuery.of(context).size.width * 2 / 3,
        padding: EdgeInsets.symmetric(
          horizontal: 24,
          vertical: 16,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: message.senderId == user.uid
                ? [
                    Color(0xff007EF4),
                    Color(0xff2A75BC),
                  ]
                : [
                    Color(0x333F4C6B),
                    Color(0x66606C88),
                  ],
          ),
          borderRadius: message.senderId == user.uid
              ? BorderRadius.only(
                  topLeft: Radius.circular(23),
                  topRight: Radius.circular(23),
                  bottomLeft: Radius.circular(23),
                )
              : BorderRadius.only(
                  topLeft: Radius.circular(23),
                  topRight: Radius.circular(23),
                  bottomRight: Radius.circular(23),
                ),
        ),
        child: Text(
          message.message,
          style: TextStyle(
            fontSize: 20.0,
            color: message.senderId == user.uid ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }
}
