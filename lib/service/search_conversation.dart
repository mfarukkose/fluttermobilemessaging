import 'package:flutter/material.dart';
import 'package:rtm/chat.dart';
import 'package:rtm/model/conversation.dart';
import 'package:rtm/model/user.dart' as userx;
import 'package:rtm/service/app_state.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rtm/service/firestore_service.dart';

class SearchConversatiton extends SearchDelegate<String> {
  List<userx.User> userList = new List();
  AppStateModel appStateModel = new AppStateModel();
  Query _ref;
  User user = FirebaseAuth.instance.currentUser;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    _ref = FirebaseDatabase.instance
        .reference()
        .child("users")
        .orderByChild("name")
        .startAt(query)
        .endAt(query + "\uf8ff");

    return FutureBuilder(
        future: _ref.once(),
        builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
          if (snapshot.hasData) {
            userList.clear();
            Map<dynamic, dynamic> values = snapshot.data.value;
            if (values != null)
              values.forEach(
                (key, values) {
                  if (key != user.uid)
                    userList.add(
                      new userx.User(
                        key: key,
                        name: values["name"],
                        phoneNumber: values["phoneNumber"],
                      ),
                    );
                },
              );
            return _buildSuggestions(userList);
          }
          return CircularProgressIndicator();
        });
  }

  Widget _buildSuggestions(List<userx.User> userList) {
    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemCount: userList.length,
      shrinkWrap: false,
      itemBuilder: (context, i) => ListTile(
        onTap: () async {
          Conversation conversation = new Conversation(
            reciever: userList[i],
          );
          FirestoreService firestoreService = new FirestoreService();

          DataSnapshot snapshot =
              await firestoreService.getConversation(conversation);

          Conversation c = Conversation.fromSnapshot(snapshot);
          Navigator.push(
            context,
            MaterialPageRoute<void>(
              builder: (context) => Chat(),
            ),
          );
          // showResults(context);
        },
        leading: Icon(Icons.person),
        title: Text(userList[i].name),
      ),
    );
  }
}
