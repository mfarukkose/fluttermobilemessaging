import 'package:flutter/foundation.dart' as foundation;
import 'package:http/http.dart' as http;
import 'package:rtm/model/conversation.dart';
import 'package:rtm/model/user.dart';
import 'dart:convert';
import '../model/message.dart';
import 'package:web_socket_channel/io.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppStateModel extends foundation.ChangeNotifier {
  void loadMessages() {}

  String hostAddress = 'http://192.168.1.55:8080/';

  logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  saveConversation(Conversation conversation) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("conversation", conversation.toString());
  }

  Future<Conversation> getConversation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String conversationText = prefs.getString("conversation");
    return Conversation.fromJson(jsonDecode(conversationText));
  }

  Future<List<Message>> fetchMessage() async {
    final response = await http.get(hostAddress + 'getMessageList');

    if (response.statusCode == 200) {
      var res = jsonDecode(response.body) as List;
      return res
          .map(
            (e) => new Message.fromJson(e),
          )
          .toList();
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<Conversation> createOrGetConversation(
      Conversation conversation) async {
    final http.Response response = await http.post(
      hostAddress + 'createOrGetConversation',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: conversation.toString(),
    );

    if (response.statusCode == 200) {
      var res = jsonDecode(response.body);
      return Conversation.fromJson(res);
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<List<Conversation>> getMyConversations() async {
    User user = await getCurrentUser();
    final response = await http
        .get(hostAddress + 'getMyConversations?userId=' + user.key.toString());

    if (response.statusCode == 200) {
      var res = jsonDecode(response.body) as List;

      return res
          .map(
            (e) => new Conversation.fromJson(e),
          )
          .toList();
    } else {
      throw Exception('Failed to load conversations!');
    }
  }

  Future<List<User>> searchUser(String userName) async {
    final response =
        await http.get(hostAddress + 'searchUser?userName=' + userName);

    if (response.statusCode == 200) {
      var res = jsonDecode(response.body) as List;
      return res
          .map(
            (e) => new User.fromJson(e),
          )
          .toList();
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future connectSocket() async {
    Conversation conversation = await getConversation();
    return IOWebSocketChannel.connect(
      "ws://localhost:8080/chat",
      headers: {"conversation": conversation.toString()},
    );
  }

  getJsonDecoded(encodedMessage) {
    var res = jsonDecode(encodedMessage) as List;
    return res
        .map(
          (e) => new Message.fromJson(e),
        )
        .toList();
  }

  setCurrentUser(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("currentUser", user.toString());
  }

  Future<User> getCurrentUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userText = prefs.getString("currentUser");
    if (userText != null) {
      return User.fromJson(jsonDecode(userText));
    } else {
      return null;
    }
  }

  signUp(User user) async {
    final http.Response response = await http.post(
      hostAddress + 'saveUser',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: user.toString(),
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.

      User user = User.fromJson(jsonDecode(response.body));
      setCurrentUser(user);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}
