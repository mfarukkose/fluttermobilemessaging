import 'package:flutter/material.dart';
import 'package:rtm/components/custom_button.dart';
import 'package:rtm/components/custom_text_field.dart';
import 'package:rtm/conversation_list.dart';
import 'package:rtm/model/user.dart';
import 'package:rtm/service/app_state.dart';

class SignUp extends StatelessWidget {
  const SignUp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String name;
    String phone;
    AppStateModel appStateModel = new AppStateModel();

    return Scaffold(
      appBar: AppBar(
        title: Text('Kayıt Ol'),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            // stops: [0.5, 0.9],
            colors: [
              Color.fromARGB(55, 245, 220, 82),
              Color.fromARGB(100, 151, 184, 85),
            ],
          ),
        ),
        height: size.height,
        width: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomTextField(
                  label: 'Ad Soyad',
                  iconData: Icons.person,
                  onChanged: (value) {
                    name = value;
                  },
                ),
                CustomTextField(
                  label: 'Telefon Numarası',
                  iconData: Icons.phone,
                  onChanged: (value) {
                    phone = value;
                  },
                ),
                CustomButton(
                  text: 'Kayıt Ol',
                  press: () {
                    if (name != null &&
                        name != '' &&
                        phone != null &&
                        phone != '') {
                      User user = new User(name: name, phoneNumber: phone);
                      appStateModel.signUp(user);
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute<void>(
                          builder: (BuildContext context) {
                            return ConversationList();
                          },
                        ),
                      );
                    } else {
                      print("nooo");
                    }
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
