import 'package:flutter/material.dart';

class Message {
  const Message({
    this.key,
    this.senderId,
    this.receiverId,
    this.conversationId,
    this.message,
    this.date,
    this.isReaded,
  });

  final String key;
  final String senderId;
  final String receiverId;
  final String conversationId;
  final String message;
  final String date;
  final bool isReaded;

  factory Message.fromJson(json) => Message(
        message: json["message"],
        senderId: json["senderId"],
        receiverId: json["receiverId"],
        conversationId: json["conversationId"],
        date: json["date"],
        isReaded: json["isReaded"],
      );

  factory Message.fromSnapshot(snapshoot) => Message(
        key: snapshoot.key,
        message: snapshoot.value["message"],
        senderId: snapshoot.value["senderId"],
        receiverId: snapshoot.value["receiverId"],
        conversationId: snapshoot.value["conversationId"],
        date: snapshoot.value["date"],
        isReaded: snapshoot.value["isReaded"],
      );

  Map<String, dynamic> toJson() => {
        'message': message,
        'senderId': senderId,
        'receiverId': receiverId,
        'conversationId': conversationId,
        'date': date,
        'isReaded': isReaded,
      };

  @override
  String toString() {
    return '{"senderId": "$senderId", "receiverId": "$receiverId", "conversationId": "$conversationId", "message": "$message", "date": "$date", "isReaded": $isReaded}';
  }
}
